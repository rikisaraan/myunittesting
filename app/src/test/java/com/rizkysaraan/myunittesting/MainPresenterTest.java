package com.rizkysaraan.myunittesting;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
//Fungsinya untuk menginisialisai framework Mockito
@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {
    //untuk membuat obyek mock yang akan menggantikan obyek yang asli.
    @Mock
    private MainPresenter presenter;
    private MainView view;

    //Method yang diberi anotasi @Before ini akan dijalankan sebelum menjalankan semua method dengan anotasi @Test
    //anotasi @After yang berfungsi sebaliknya dari anotasi @Before, yaitu untuk menginisialisai method yang akan dijalankan setelah method dengan anotasi @Test.
    @Before
    public void setUp(){
        view = mock(MainView.class);
        presenter = new MainPresenter(view);
    }

    //Anotasi ini digunakan pada method yang akan ditest.
    @Test
    public void testVolumeWithIntegerInput() {
        double volume = presenter.volume(2, 8, 1);
        //assertEquals merupakan fungsi dari JUnit yang digunakan untuk memvalidasi output yang diharapkan dan output yang sebenarnya.
        assertEquals(16, volume, 0.0001);
    }

    @Test
    public void testVolumeWithDoubleInput() {
        double volume = presenter.volume(2.3, 8.1, 2.9);
        assertEquals(54.026999999999994, volume, 0.0001);
    }

    @Test
    public void testVolumeWithZeroInput() {
        double volume = presenter.volume(0, 0, 0);
        assertEquals(0.0, volume, 0.0001);
    }

    @Test
    public void testCalculateVolume() {
        presenter.calculateVolume(11.1, 2.2, 1);
        //Verify Digunakan untuk memeriksa metode dipanggil dengan arguman yang diberikan. Verify merupkan fungsi dari framework Mockito
        //any Merupakan fungsi dari Mockito yang digunakan untuk mencocokan argumen yang fleksibel. any digunakan bareng dengan verify.
        verify(view).showVolume(any(MainModel.class));
    }

   /* @Test
    public void volume() {
    }

    @Test
    public void calculateVolume() {
    }*/
}